import { all, take, put } from 'redux-saga/effects';

const initialState = {
  pokemonList: []
};

export const moduleName = 'add';
export const appName = 'pokemon';
export const FETCH_ALL_REQUEST = `${appName}/${moduleName}/FETCH_ALL_REQUEST`;

export const SET_POCKEMONS = `${appName}/${moduleName}/SET_POCKEMONS`;

export default function reducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case FETCH_ALL_REQUEST:
    case SET_POCKEMONS:
      return { ...state, pokemonList: payload };
    default:
      return state;
  }
}

// action creator
export const fetchAll = function () {
  return { type: FETCH_ALL_REQUEST };
};

export const fetchPokemonList = function* () {
  yield take(FETCH_ALL_REQUEST);

  const pokemonLimit = yield 1000;
  const fetchData = yield fetch(`https://pokeapi.co/api/v2/pokemon/?limit=${pokemonLimit}`);
  yield put({
    type: SET_POCKEMONS,
    payload: fetchData.data.results
  });
};

export function* saga() {
  yield all([
    fetchPokemonList()
  ]);
}
