import { all } from 'redux-saga/effects';

import { saga as pokemonSaga } from '../ducks/pokemon';

export default function* rootSaga() {
  yield all([pokemonSaga()]);
}
