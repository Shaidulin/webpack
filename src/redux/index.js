import { createStore, applyMiddleware, compose } from 'redux';
import logger from 'redux-logger';
import { persistStore, autoRehydrate } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './saga';
import reducer from './reducer';

const sagaMiddleware = createSagaMiddleware();

const enhancer = applyMiddleware(sagaMiddleware, logger);

const store = createStore(reducer, compose(enhancer, autoRehydrate()));

sagaMiddleware.run(rootSaga);

function save() {
  const state = store.getState();

    fetch('/api/data', { // eslint-disable-line
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        decks: state.decks,
        cards: state.cards
      })
    });
}

persistStore(store);

export default store;
