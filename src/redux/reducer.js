import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import pokemonReducer, { moduleName as userModule } from '../ducks/pokemon';

export default combineReducers({
  router,
  [userModule]: pokemonReducer
});
